/**
 *
 * Manipulating the DOM exercise.
 * Exercise programmatically builds navigation,
 * scrolls to anchors from navigation,
 * and highlights section in viewport upon scrolling.
 *
 * Dependencies: None
 *
 * JS Version: ES2015/ES6
 *
 * JS Standard: ESlint
 *
*/

/**
 * Comments should be present at the beginning of each procedure and class.
 * Great to have comments before crucial code sections within the procedure.
*/

/**
 * Define Global Variables
 *
*/
let sectionName;
const fragment = document.createDocumentFragment();
/**
 * End Global Variables
 * Start Create Nav Menu Function
 *
*/
document.addEventListener("DOMContentLoaded", buildNavMenu);
const ul = document.querySelector('#navbar__list');
console.log(ul);
console.log(li);

function buildNavMenu() {
    const sections = document.getElementsByTagName("section");
    console.log(sections);
    for (let section of sections) {
        var li = document.createElement('li');
        li.setAttribute('class', 'navbar__menu');
        const linkSection = `<a href="#${section.id}" class ="navbar__menu menu__link"> ${section.dataset.nav} </a>`;
        const link = document.createElement('a');
        link.innerText = `${section.dataset.nav}`;
        link.setAttribute('href', `#${section.id}`);
        link.setAttribute('class', 'menu__link');
        li.setAttribute('id', 'id' + section.id);
        li.appendChild(link);
        // ul.appendChild(li);
        fragment.appendChild(li);
    }
    ul.appendChild(fragment)
}

/**
 * End Create Nav Menu Function
 * Begin Main Functions
 *
*/
document.addEventListener('Click', clickForActivateClass)
function clickForActivateClass() {

}
// build the nav


// Add class 'active' to section when near top of viewport


// Scroll to anchor ID using scrollTO event


/**
 * End Main Functions
 * Begin Events
 *
*/

// Build menu 

// Scroll to section on link click

// Set sections as active

